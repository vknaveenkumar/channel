### start the application
Go to root folder : 
1) npm install
2) npm start

### install the json-server
first install :
1) npm install -g json-server

### start the json-server
Go src/json
json-server db.json -p 3001


### to run test 
npm run test 
