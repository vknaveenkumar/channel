import React from 'react';
import { shallow, mount } from 'enzyme';
import {Dashboard} from '../../containers/Dashboard';
import * as apis from '../../../api/api';
import jsonData from '../../../json/db.json'
/**
 * below testcase is used to mock the promise and check wheather componentDIdMount is called and 
 * check getData() is called
 */
const responseObj = { data: jsonData.channels };

apis.getData = jest.fn(() => Promise.resolve(responseObj));

describe('When Dashboard component is given', () => {
    let didMountSpy;
    let wrapper;
    beforeEach(() => {
        didMountSpy = jest.spyOn(Dashboard.prototype, 'componentDidMount');
    });

    afterEach(() => {
        didMountSpy.mockClear();
    });

    it('should NOT render the component', () => {
        expect(wrapper).not.toBeDefined();
    });

    it('should NOT have called "ComponentDidMount()" ', () => {
        expect(didMountSpy).toHaveBeenCalledTimes(0);
    });

    describe('When render() is called', () => {
        beforeEach(() => {
            wrapper = mount(<Dashboard />);
        });

        it('should render the component', () => {
            expect(wrapper).toHaveLength(1);
        });

        it('should have called "ComponentDidMount()" ', () => {
            expect(didMountSpy).toHaveBeenCalledTimes(1);
        });

        it('should have called "getData()"', () => {
            expect(apis.getData).toHaveBeenCalled();
        });
    });

});


