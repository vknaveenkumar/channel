import React from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from "@material-ui/core/styles";
import axios from 'axios';
import Typography from '@material-ui/core/Typography';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux'
import { getData } from '../../api/api'
import Container from '@material-ui/core/Container';
import moment from 'moment'
import Navbar from '../components/Navbar/Navbar'
import Layout from '../components/Layout/Layout'
import { BASE_URL } from '../../config/env'

//demo actions
import * as actions from '../actions/channel-actions';

/**
 * @param {*} theme 
 * This is  Dashboard Component
 */


const styles = theme => ({
    root: {
        backgroundColor: "red"
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },


});

export class Dashboard extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            channel: []
        }
    }


    componentDidMount() {
        this.props.actions.getData();
    }

    render() {
        const { classes } = this.props;
        const { channels } = this.props

        return (
            <React.Fragment>
                <Navbar
                    appBarColor={'#1976d2'}
                    {...this.props}
                />
                <div style={{ backgroundColor: '#f5f5f5', paddingTop: '2%' }}>
                    <Container >
                        {
                            Object.keys(channels).length > 0 && Object.keys(channels).map((data) => {
                                return <React.Fragment>
                                    <Typography variant='h6' style={{ margin: '2%' }}>
                                        {moment(data).format('dddd, MMMM Do YYYY')}
                                    </Typography>

                                    <Layout items={channels[data]} />
                                </React.Fragment>
                            })
                        }
                    </Container >
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return { channels: state.channels.channel };
};

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(Dashboard))

