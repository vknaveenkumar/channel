import { ADD_CHANNEL } from "../constants/action-types";
import axios from 'axios';
import { BASE_URL } from '../../config/env'
//demo data
export const getData = () => {
    return dispatch => {
        axios.get(`${BASE_URL}/channels`).then(response => {
            if (response) {
                const channel = response.data.reduce((acc, data) => {
                    let { time } = data;
                    let splicedData = time.split(' ')
                    return { ...acc, [splicedData[0]]: [...(acc[splicedData[0]] || []), data] };
                }, {});
                dispatch({ type: ADD_CHANNEL, payload: channel });
            }
        })
    }
}
