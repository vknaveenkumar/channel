import React from 'react';
import Typography from '@material-ui/core/Typography';



function logo(props) {
    return (
        <div>
            <Typography style={{color:'white',position:'absolute',top:'15px',left:'15px'}} component="h1" variant="h5">
                MOVIESTREAM
            </Typography>
        </div>
    );
}

export default logo;