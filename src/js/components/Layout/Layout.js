import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '../Card/Card'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing(2),
  },
}));

/**
 * 
 * @param {*} props
 * This component to specify the layout of the card 
 */
export default function SpacingGrid(props) {

  const classes = useStyles();

  const { items, direction } = props;

  return (
    <Grid container className={classes.root} >
      <Grid item xs={12}>
        <Grid container justify="flex-start" direction={direction} spacing={1}>
          {items.map((item) => (
            <Grid key={item.id} item>
              <Card item={item} {...props} />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
}
