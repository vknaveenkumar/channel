import React from 'react';
import Card from '@material-ui/core/Card';
import moment from 'moment'
import CardContent from '@material-ui/core/CardContent';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from "@material-ui/core/styles";



const styles = theme => ({
    root: {
        display: 'flex',
    },
    details: {
        display: 'flex',
        flexDirection: 'row',
    },
    icon: {
        flexBasis: '10%'
    },
    description: {
        flexBasis: '40%'
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
      },
    user: {
        flexBasis: '15%'
    },
    time: {
        flexBasis: '35%'
    },
    title: {
        fontSize: '18px',
        fontWeight: 'bold'
    },
    username: {
        fontSize: '12px',
        fontWeight: 'bold'
    }
});


function MovieStreamCard({ classes, item }) {

    return (
        <Card >
            <CardContent className={classes.details}>
                <div className={classes.icon}>
                    <IconButton
                        edge="end"
                        aria-label="account of current user"
                        aria-haspopup="true"
                        color="inherit"
                    >
                        {<AccountCircle />}
                    </IconButton>
                </div>
                <div className={classes.description}>
                    <Typography className={classes.title}   >
                        {item.title}
                    </Typography>
                    <Typography variant="p"  >
                        {item.description}
                    </Typography>
                </div>
                <div className={classes.user}>
                    <IconButton
                        edge="end"
                        aria-label="account of current user"
                        aria-haspopup="true"
                        color="inherit"
                    >
                        <Avatar alt={item.instructorName} src={'https://material-ui.com/static/images/avatar/3.jpg'} className={classes.small}/>
                        <Typography variant="p" className={classes.username} style={{ paddingLeft: '10px' }}  >
                            {item.instructorName}
                        </Typography>
                    </IconButton>
                </div>
                <div className={classes.time}>
                    <Typography variant="p" className={classes.username} style={{ paddingLeft: '10px' }}  >
                        {`${moment(item.time).format('h:mm')} - ${moment(item.time).add(1, 'hours').format('h:mm')} EDT`}
                    </Typography>

                </div>
            </CardContent>
        </Card>
    );
}

export default withStyles(styles)(MovieStreamCard);