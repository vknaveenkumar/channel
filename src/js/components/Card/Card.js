import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import { withStyles } from "@material-ui/core/styles";
import CardContent from './CardContent'


const styles = () => ({
  cardWidth: {
    minWidth: 300,
  },
  media: {
    height: 140,
  },
  cardBody: {

  }
});



function MediaCard(props) {

  const { classes,item } = props


  return (
    <Card className={classes.cardWidth}>
      <CardActionArea>
        <CardContent item={item} />
      </CardActionArea>
    </Card>
  );
}


export default withStyles(styles)(MediaCard);
