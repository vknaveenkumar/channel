import { combineReducers } from 'redux';
import channels from './index';


export default combineReducers({
    channels
});