import { ADD_CHANNEL } from "../constants/action-types";

const initialState = {
  channel: {}
};



export default (state = initialState, action) => {

  switch (action.type) {
    case ADD_CHANNEL:
      return {
        ...state, channel: {...action.payload}
      };
    default:
      return state;
  }

};


