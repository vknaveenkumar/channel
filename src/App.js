import React, { Suspense } from 'react';
//import logo from './logo.svg';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import { Provider } from "react-redux";
import './App.css';
import store from "./js/store/index";
const Dashboard = React.lazy(() => import('./js/containers/Dashboard'));

function App() {
  return (
    <div>
      <Provider store={store}>
        <Suspense fallback={<div>Loading...</div>}>
          <Router>
            <Switch>
              <Route exact path="/" component={Dashboard} />
            </Switch>
          </Router>
        </Suspense>
      </Provider>
    </div>
  );
}

export default App;
