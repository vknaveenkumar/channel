import axios from 'axios';
import { BASE_URL } from '../config/env'

export const getData = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${BASE_URL}/channels`).then(function (response) {
            resolve(response)
        }).catch(function (error) {
            reject(error)
        });
    });
}